const Joi = require("joi");

const validarUsuario = async (req, res, next) => {
  const schema = Joi.object({
    id: Joi.number().required(),
    nombre: Joi.string().required(),
  });
  
  try {
    await schema.validateAsync(req.body);
    return next();
  } catch (err) {
    return res.status(400).send({
        mensaje: "Datos de entrada no válidos"
    })
  }
};

module.exports = validarUsuario;
