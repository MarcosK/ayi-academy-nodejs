const get = (req, res) => {
    //MOCK LLAMADA BD
    const personas = [
        {
            id: 1,
            nombre: 'Fernando'
        },
        {
            id: 2,
            nombre: 'Franco'
        }
    ]

    const persona = personas.find(p => p.id == req.params.id);
    res.status(200).send(persona);
};

const post = (req, res) => {
    const persona = req.body;
    console.log(persona);
    // INSERT BASE DE DATOS
    res.status(200).send({
        mensaje: `Se agregó la persona con id ${persona.id} con éxito en la Base de Datos`
    })
}

module.exports = {
    get,
    post
}