const userController = require('../controllers/userController');
const express = require('express');
const router = express.Router();

router.get('/:id', userController.get);
router.post('/', userController.post);

module.exports = router;